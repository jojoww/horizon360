#include <QApplication>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QQmlContext>
#include <QQmlComponent>
#include <QDebug>
#include <QScreen>
#include <QQuickStyle>
#include "turn360manager.h"
#include "graphicsgenerator.h"


int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QQuickStyle::setStyle("Material");

    Turn360Manager turn360;

    GraphicsGenerator *img = new GraphicsGenerator;
    img->setManager(&turn360);
    turn360.setGraphicsGenerator(img);

    engine.rootContext()->setContextProperty("manager", &turn360);
    engine.addImageProvider(QLatin1String("images"), img);


    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
