#include "turn360manager.h"
#include <QDebug>
#include <QCoreApplication>
#include <QtConcurrent/QtConcurrent>



/**
 * @brief Constructor
 */
Turn360Manager::Turn360Manager()
{
}



/**
 * @brief Destructor
 */
Turn360Manager::~Turn360Manager()
{

}



/**
 * @brief Set the GraphicsGenerator for GUI. Wil be updated by the manager with current pano data.
 * @param img
 */
void Turn360Manager::setGraphicsGenerator(GraphicsGenerator *img)
{
    m_img = img;
}



/**
 * @brief Getter for the pano object
 * @return
 */
PanoImage* Turn360Manager::getPano()
{
    return &m_pano;
}



/**
 * @brief Load a pano file and create pano object
 * @param path
 */
void Turn360Manager::openFile(QUrl path)
{
    m_path = path.toLocalFile();
    qDebug() << "Manager - Open panorama " << m_path;
    m_pano = PanoImage(m_path);
}



/**
 * @brief Getter for coordinate converter
 * @return
 */
CoordinateConverter* Turn360Manager::getCc()
{
    return &m_cc;
}



/**
 * @brief Save result in high resolution, in same folder, with suffix
 */
void Turn360Manager::saveResult()
{
    int width = m_pano.getWidth();
    qDebug() << "Manager - save result with width " << width;
    m_result = getTilted(width);
    m_result.save(m_path+"_corrected.jpg");
}



/**
 * @brief Sample a certain view from given perspective/angle.
 * @param width
 * @param height
 * @param x
 * @param y
 * @param angle
 * @param i The number of the sequence; used for output file name
 */
void Turn360Manager::sample2DImage(int width, int height, double x, double y, double angle, int i)
{
    QVector<double> parameters;
    parameters << width << height << x  << y  << angle << i;
    QFuture<void> future = QtConcurrent::run(&sample2DImageHelper, parameters, this);

}



/**
 * @brief Helper for threading.
 * @param pp Contains all parameters as double list. See sampleImage(...)
 */
void Turn360Manager::sample2DImageCall(QVector<double> &pp)
{
    emit currentPos(pp[2], pp[3]);
    if(!QDir(m_path+"_animation").exists())
    {
        QDir().mkdir(m_path+"_animation");
    }
    QString suffix = QString("%1").arg((int)pp[5], 5, 10, QChar('0'));

    m_pano.sample2DImage(pp[0],
            pp[1],
            pp[2] * (double)m_pano.getWidth(),
            pp[3] * (double)m_pano.getWidth() / 2.,
            pp[4],
            m_cc).save(m_path+"_animation/" + suffix + ".jpg");
    emit finished();
}



/**
 * @brief Helper for threading
 * @param pp See sampleImage()
 * @param tm Reference to "this" object
 */
void Turn360Manager::sample2DImageHelper(QVector<double> &pp, Turn360Manager *tm)
{
    tm->sample2DImageCall(pp);
}



/**
 * @brief Update the angle values (provided by GUI)
 * @param x
 * @param y
 * @param z
 */
void Turn360Manager::updateAngle(double x, double y, double z)
{

    m_cc.setAngle(x, y, z);
}



/**
 * @brief Transform and return the panorama
 * @param width Desired output size
 * @return
 */
QImage Turn360Manager::getTilted(int width)
{
    m_cc.setWidth(width);
    return m_pano.getTilted(width, m_cc);
}


void Turn360Manager::callFfmpeg()
{

    qDebug() << "Call ffmpeg";
    QString folder = m_path + "_animation/";

    QString processName = "./ffmpeg.exe";
    if(!QFile(processName).exists())
    {
        processName = "ffmpeg";
    }

    processName += " -framerate 30 -i '" + folder +"/%05d.jpg' '"+folder+"/output.mp4'";

    qDebug() << "Run process " << processName;
    QProcess process;
    process.start(processName);
    process.waitForFinished();
    QString output(process.readAllStandardOutput());
    qDebug() << output;
}
