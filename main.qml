import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.1
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.2
// Update the main (data) image



Window
{
    property real customScaleExtern: 1.
    property int numJobs: 0;

    Connections {
        target: manager
        onFinished: {
            console.log("Job done");
            numJobs -= 1
            if(numJobs === 0)
            {
                busy = false;
                animationCurrent.visible = false
                manager.callFfmpeg()
            }
        }
    }

    Connections {
        target: manager
        onCurrentPos: { handleCurrentPos(x, y) }
    }

    function handleCurrentPos(x, y)
    {
        console.log("HANDLE CURRENT ", x, y);
        animationCurrent.x = x * panoRawImage.width
        animationCurrent.y = y * panoRawImage.height
    }

    function createAnimation()
    {
        busy = true;
        numJobs = animationFrames.value;
        var flip = animationDirection.checked;
        var dX = animationEnd.x - animationStart.x;
        var dY = animationEnd.y - animationStart.y;
        var dA = (animationAngleEnd.value - animationAngleStart.value)

        if(flip)
        {
            dX = -(panoRawImage.width - dX);
            //dY *= -1;
        }

        if( animationEnd.x < animationStart.x)
        {
            dX += panoRawImage.width
        }

        if( animationEnd.y < animationStart.y)
        {
            //dY += panoRawImage.height
        }

        console.log("DX", dX)
        console.log("DY", dY)

        var stepX = dX / animationFrames.value;
        var stepY = dY / animationFrames.value;
        var stepA = (dA / animationFrames.value) / 360 * 2. * 3.142;
        console.log(stepX, stepY)

        animationCurrent.visible = true;
        var angle = animationAngleStart.value / 360 * 2. * 3.142
        var curX = animationStart.x;
        var curY  = animationStart.y;
        for(var i = 0; i < animationFrames.value; i++)
        {
            curX += stepX
            curY += stepY
            if(curX < 0) curX += panoRawImage.width;
            if(curY < 0) curY += panoRawImage.height;
            if(curX >= panoRawImage.width) curX -= panoRawImage.width;
            if(curY >= panoRawImage.height) curY += panoRawImage.height;
            angle += stepA;


            manager.sample2DImage(resolutionX.value,
                                resolutionY.value,
                                curX / panoRawImage.width,
                                curY / panoRawImage.height,
                                angle,
                                i
                                )

        }

    }

    function updateImage()
    {
        var newImage = "image://images/1_" + Math.random(10000)
        panoRawImage.source = newImage;
        //panoRawImage.width = 800 * panoRawImage.customScale
        root.title = "Work on: " + fileDialog.fileUrl
    }


    function calculatePreview()
    {
        var newImage = "image://images/2_" + Math.random(10000)
        panoTiltedImage.source = newImage;
        configureAnimation = true
        //panoTiltedImage.width = 800 * panoTiltedImage.customScale
    }


    function calculateResult()
    {
        saveButton.enabled = false
        manager.saveResult()
    }


    function updateHorizon()
    {
        manager.updateAngle(xSlider.value / 360. * Math.PI , ySlider.value  / 360. * Math.PI, zSlider.value * 2.0 / 360. * Math.PI)
        saveButton.enabled = true
        if(autoPreview.checked)
        {
             calculatePreview()
        }

        updateImage()
    }


    function loadPano()
    {
        fileDialog.open()
    }

    id: root
    visible: true
    width:  1300 * customScale
    height: 900 * customScale
    property real customScale: 1.
    property bool configureAnimation: false
    title: qsTr("Tilt your 360 Pano")
    property bool busy : false;

    FileDialog
    {
        id: fileDialog
        title: "Please choose one image file"
        nameFilters: [ "Image files (*.jpg *.png *.tif)"]
        folder: "file:///F:/___Bilder/___2018/2018 09 September/2018 09 23 - 10 11 Vietnam/360_stitch"
        selectMultiple: true
        onAccepted:
        {
            console.log("You chose: " + fileDialog.fileUrl)
            manager.openFile(fileDialog.fileUrl)
            saveButton.enabled = true
            updateImage()
            close()
        }
        onRejected:
        {
            close()
        }
    }

    GridLayout
    {
        id: fullViewGrid
        columns: 4
        anchors.fill: parent
        enabled: !busy


        Rectangle
        {
            width:10
            height: controlGrid.height;
            color: "transparent";
        }

        GridLayout
        {
            columns: 1
            id: controlGrid
            property real customScale: 1.
            Layout.preferredWidth: 400
            Layout.maximumWidth: 400;

            Button
            {
                text: "Load image"
                onClicked: loadPano()
            }

            Slider
            {
                id: xSlider
                from: 0
                to: 360;
                value: 0
                onValueChanged: updateHorizon()
                Layout.fillWidth: parent
                stepSize: 1
            }

            Slider
            {
                id: ySlider
                from: 0;
                to: 360;
                value: 0
                onValueChanged: updateHorizon()
                Layout.fillWidth: parent
                stepSize: 1
            }

            Slider
            {
                id: zSlider
                from: 0;
                to: 360;
                value: 0
                onValueChanged: updateHorizon()
                Layout.fillWidth: parent
                stepSize: 1
            }

            GridLayout
            {
                rows: 1

                CheckBox
                {
                    id: autoPreview
                    text: "Auto preview?"
                    checked: false
                }

                Button
                {
                    enabled: autoPreview.checked == false && saveButton.enabled
                    text: "Preview"
                    onClicked: calculatePreview()
                }
            }



            Button
            {
                id: saveButton
                enabled: false
                text: "Save"
                onClicked: calculateResult()
            }

            GridLayout
            {
                enabled: configureAnimation
                columns: 2
                Label
                {
                    text: "Number of frames"
                }
                SpinBox
                {
                    editable: true
                    id: animationFrames
                    from: 2
                    to: 10000
                    value: 150
                }


                Label
                {
                    text: "Width"
                }
                SpinBox
                {
                    editable: true
                    id: resolutionX
                    from: 100
                    to: 10000
                    value: 1920
                }


                Label
                {
                    text: "Height"
                }
                SpinBox
                {
                    editable: true
                    id: resolutionY
                    from: 100
                    to: 10000
                    value: 1080
                }

                Label
                {
                    text: "Angle at start"
                }
                SpinBox
                {
                    editable: true
                    id: animationAngleStart
                    from: 10
                    to: 180
                    value: 100
                }

                Label
                {
                    text: "Angle at end"
                }
                SpinBox
                {
                    editable: true
                    id: animationAngleEnd
                    from: 10
                    to: 180
                    value: 100
                }

                Label
                {
                    text: "Going to left"
                }
                CheckBox
                {

                    id: animationDirection
                    checked: false
                }
                Button
                {
                    text: "Create animation"
                    onClicked: createAnimation()
                }

            }
        }








        Rectangle
        {
            width:10
            height: controlGrid.height;
            color: "transparent";
        }






        GridLayout
        {
            columns: 1
            id: imageGrid
            enabled: !busy

            Image
            {
                id: panoRawImage
                source: "image://images/default"
                property real customScale: 1.
                width: 800
                height: panoRawImage.width / 2

            }

            Image
            {
                id: panoTiltedImage
                source: "image://images/default"
                property real customScale: 1.
                width: 800
                height: panoTiltedImage.width / 2

                Rectangle
                {
                    id: animationStart
                    visible: configureAnimation
                    color: "#66ccff"
                    border.color: "red"
                    border.width: 2
                    width: 20
                    height: 20
                    x:100;
                    y:200;

                    MouseArea
                    {
                        id: dragAreaStart
                        anchors.fill: parent

                        drag.target: parent
                        drag.minimumX : -10
                        drag.maximumX : 810

                        drag.minimumY : -10
                        drag.maximumY : 410
                    }
                }

                Rectangle
                {
                    id: animationEnd
                    visible: configureAnimation
                    color: "#ffccaa"
                    border.color: "red"
                    border.width: 2
                    width: 20
                    height: 20
                    x:700;
                    y:200;

                    MouseArea
                    {
                        id: dragAreaEnd
                        anchors.fill: parent

                        drag.target: parent
                        drag.minimumX : -10
                        drag.maximumX : 810

                        drag.minimumY : -10
                        drag.maximumY : 410
                    }

                    onXChanged: {
                        console.log("New position: " + x + " " + y)
                    }
                }


                Rectangle
                {
                    id: animationCurrent
                    visible: false
                    color: "red"
                    border.color: "red"
                    border.width: 2
                    width: 20
                    height: 20
                    x:100;
                    y:200;


                }
            }
        }


    }

    Component.onCompleted:
    {
        console.log("Scale is " + customScaleExtern)
        root.customScale = customScaleExtern
        controlGrid.customScale = customScaleExtern
        panoRawImage.customScale = customScaleExtern
        panoTiltedImage.customScale = customScaleExtern
        //imageGrid.customScale = customScaleExtern
        console.log("Scale of image: " + imageGrid.width)
    }
}
