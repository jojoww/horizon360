#include "panoimage.h"
#include "horizonfunction.h"
#include <QDebug>
#include <QVector2D>
#include "qmath.h";


/**
 * @brief Invalid default constructor
 */
PanoImage::PanoImage()
{
    m_image = QImage();
}



/**
 * @brief Constructor with valid pano image
 * @param path Path to file
 */
PanoImage::PanoImage(QString path)
{
    m_image = QImage(path);
    m_sampled = m_image.copy();
    m_sampleWidth = m_image.width();
    if(m_image.width() == 0 || (m_image.height() * 2) != m_image.width())
    {
        qDebug() << "No proper image selected: " << path;
    }
}



/**
 * @brief Getter for image
 * @return
 */
QImage PanoImage::getImage() const
{
    return m_image;
}



/**
 * @brief Getter for a scaled image
 * @param width
 * @return
 */
QImage PanoImage::getImage(int width) const
{
    return m_image.scaled(width, width/2);
}



/**
 * @brief Getter for an image position (in image space) with applied transformation
 * @param ax
 * @param ay
 * @param cc Holding information about the transformation
 * @return
 */
QColor PanoImage::getColor(double x, double y, CoordinateConverter &cc)
{
    double sampleX, sampleY;
    cc.rotate(x, y, sampleX, sampleY);
    return m_sampled.pixel(((int)sampleX + m_image.width()) % m_image.width(),
                           ((int)sampleY + m_image.height()) % m_image.height());
}



/**
 * @brief Apply transformation to the full image
 * @param width Desired output width
 * @param cc
 * @return
 */
QImage PanoImage::getTilted(int width, CoordinateConverter &cc)
{
    setSampleWidth(width);

    QImage result(width, width/2, QImage::Format_ARGB32);
    if(result.width() != width)
    {
        qDebug() << "Error! Could not create result of width " << width;
    }

    for(int x = 0; x < width; x++)
    {
        for(int y = 0; y < width/2; y++)
        {
            result.setPixel(x, y, getColor(x, y, cc).rgba());
        }
    }

    return result;
}



/**
 * @brief Sample 2D image from within the 360-sphere
 * @param width Desired output width
 * @param height Desired output height
 * @param x Position of the image that should be center
 * @param y Position of the image that should be center
 * @param angle Horizontal field of view in 0-pi
 * @param cc Transformation
 * @return Patch from the transformed pano
 */
QImage PanoImage::sample2DImage(int width, int height, double x, double y,  double angle, CoordinateConverter &cc)
{
    qDebug() << angle;
    cc.setWidth(m_image.width());
    setSampleWidth(m_image.width());

    QImage img(width, height, QImage::Format_ARGB32);
    QVector4D p1 = cc.polarToCarth(M_PI_2, 0);
    QVector4D p2 = cc.polarToCarth(M_PI_2, (angle / 2.));
    p2 /= p2.x();
    double pixelWidth = (p2 - p1).length() / double(width/2);
    QVector4D pos, posR;

    double xpos = 0;
    double ypos = 0;
    double outX, outY;

    xpos = - pixelWidth * double(width/2);
    double theta, phi;
    double angleZ = x / float(m_image.width()) * 2. * M_PI;
    double angleY = y / float(m_image.height()) * M_PI - M_PI_2;
    QMatrix4x4 m = cc.rotationMatrix(QVector3D(0, 0, 1), angleZ) * cc.rotationMatrix(QVector3D(0, 1, 0), angleY);

    for(int x = 0; x < width; x++)
    {
        xpos += pixelWidth;
        ypos = - pixelWidth * double(height/2);
        for(int y = 0; y < height; y++)
        {
            ypos += pixelWidth;
            pos = QVector4D( 1., xpos, ypos, 0);
            posR = m * pos;
            cc.carthToImageSpace(posR, phi, theta);

            outX = theta;
            outY = phi;
            if(outX >= 0 && outY >= 0 && outX < m_image.width() && outY < m_image.height())
            {
                img.setPixel(x, height - y - 1, getColor(outX, outY, cc).rgba());
            }
        }
    }

    return img;
}



/**
 * @brief Getter for original image width
 * @return
 */
int PanoImage::getWidth()
{
    return m_image.width();
}



/**
 * @brief Set the sample width.
 * This is like a global setting - from now on, all calculations happen on the resampled image
 * @param width
 */
void PanoImage::setSampleWidth(int width)
{
    if(width != m_sampleWidth)
    {
        m_sampleWidth = width;
        m_sampled = m_image.scaled(width, width/2);
    }
}
