#ifndef GRAPHICSGENERATOR_H
#define GRAPHICSGENERATOR_H

#include <QQuickImageProvider>
#include <QPixmap>
#include <QImage>

#include "turn360manager.h"
class Turn360Manager;

class GraphicsGenerator : public QQuickImageProvider
{
public:
    void setManager(Turn360Manager *turn360);

    ~GraphicsGenerator();

    GraphicsGenerator()
        : QQuickImageProvider(QQuickImageProvider::Pixmap)
    {
    }

    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize);

private:
    Turn360Manager *m_turn360;
};

#endif // GRAPHICSGENERATOR_H
