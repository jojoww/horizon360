#ifndef PANOIMAGE_H
#define PANOIMAGE_H
#include <QString>
#include <QImage>
#include "horizonfunction.h"
#include "coordinateconverter.h"
class PanoImage
{
public:
    PanoImage();
    PanoImage(QString path);

    QImage getImage() const;
    QImage getImage(int width) const;
    QImage sample2DImage(int width, int height, double x, double y, double angle, CoordinateConverter &cc);

    QImage getTilted(int width, CoordinateConverter &cc);
    int getWidth();
    void setSampleWidth(int width);
private:
    QImage m_image;
    QImage m_sampled;
    int m_sampleWidth;
    QColor getColor(double x, double y, CoordinateConverter &cc);
};

#endif // PANOIMAGE_H
