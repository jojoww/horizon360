#include "graphicsgenerator.h"
#include <QPainter>
#include "coordinateconverter.h"


/**
 * @brief Setter for the graphics generator
 * @param turn360
 */
void GraphicsGenerator::setManager(Turn360Manager *turn360)
{
    m_turn360 = turn360;
}



/**
 * @brief Destructor
 */
GraphicsGenerator::~GraphicsGenerator()
{

}



/**
 * @brief Actual function to generate images for the GUI
 * @param id
 * @param size
 * @param requestedSize
 * @return
 */
QPixmap GraphicsGenerator::requestPixmap(const QString &id, QSize *size, const QSize &requestedSize)
{
    // Desired output - fixed, otherwise maybe too expensive. TODO: maybe defined by user?
    int width = 800;

    if(id=="default")
    {
        // No image loaded yet
        QPixmap d(width, width/2);
        d.fill(QColor(200, 205, 210));
        return d;
    }

    QStringList splitted =id.split("_");
    m_turn360->getCc()->setWidth(width);

    if(splitted[0] == "1")
    {
        // Get the original image and paint a horizon line
        QImage pano = m_turn360->getPano()->getImage(width);
        double x, y;
        for(int i = 0; i < width; i ++)
        {
            m_turn360->getCc()->rotate(i, width/4, x, y);
            pano.setPixel(x, y, QColor(255,0,0).rgba());
        }
        return QPixmap::fromImage(pano);
    }
    else
    {
        // The preview for the tilted
        QImage tilted = m_turn360->getTilted(width);
        return QPixmap::fromImage(tilted);
    }
}
