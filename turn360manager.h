#ifndef TURN360MANAGER_H
#define TURN360MANAGER_H

#include <QObject>
#include <QImage>
#include "graphicsgenerator.h"
#include "panoimage.h"
#include "horizonfunction.h"
#include "coordinateconverter.h"
class GraphicsGenerator;


class Turn360Manager : public QObject
{
    Q_OBJECT
public:
    Turn360Manager();
    ~Turn360Manager();
    void setGraphicsGenerator(GraphicsGenerator *img);
    PanoImage* getPano();
    Q_INVOKABLE void openFile(QUrl path);
    Q_INVOKABLE void updateAngle(double x, double y, double z);
    Q_INVOKABLE void saveResult();
    Q_INVOKABLE void sample2DImage(int width, int height, double x, double y, double angle, int i);
    Q_INVOKABLE void callFfmpeg();
    QImage getTilted(int width);
    CoordinateConverter* getCc();

private:
    GraphicsGenerator *m_img;
    PanoImage m_pano;
    QImage m_result;
    QString m_path;
    CoordinateConverter m_cc;
    void sample2DImageCall(QVector<double> &pp);
    static void sample2DImageHelper(QVector<double> &pp, Turn360Manager *tm);

signals:
    void finished();
    void currentPos(double x, double y);
};

#endif // TURN360MANAGER_H
