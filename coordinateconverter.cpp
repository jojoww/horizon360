#include "coordinateconverter.h"

#include "qmath.h"

CoordinateConverter::CoordinateConverter()
{

}

void CoordinateConverter::setWidth(int width)
{
    this->width = width;
}


QMatrix4x4 CoordinateConverter::rotationMatrix(QVector3D axis, double angle)
{
    axis.normalize();
    float s = qSin(angle);
    float c = qCos(angle);
    float oc = 1.0 - c;

    return QMatrix4x4(  oc * axis[0] * axis[0] + c,           oc * axis[0] * axis[1] - axis[2] * s,  oc * axis[2] * axis[0] + axis[1] * s,  0.0,
                        oc * axis[0] * axis[1] + axis[2] * s,  oc * axis[1] * axis[1] + c,           oc * axis[1] * axis[2] - axis[0] * s,  0.0,
                        oc * axis[2] * axis[0] - axis[1] * s,  oc * axis[1] * axis[2] + axis[0] * s,  oc * axis[2] * axis[2] + c,           0.0,
                        0.0,                                0.0,                                0.0,                                1.0);
}

void CoordinateConverter::setAngle(double angleX, double angleY, double angleZ)
{
    //qDebug() << "New angle is " << angleX << angleY << angleZ;
    QMatrix4x4 mX = rotationMatrix(QVector3D(1, 0, 0), angleX);
    QMatrix4x4 mY = rotationMatrix(QVector3D(0, 1, 0), angleY);
    QMatrix4x4 mZ = rotationMatrix(QVector3D(0, 0, 1), angleZ);
    m = mZ * mY * mX;
    qDebug() << m;
}


QVector4D CoordinateConverter::polarToCarth(double theta, double phi)
{

    return QVector4D(qSin(theta) * qCos(phi), qSin(theta) * qSin(phi), qCos(theta), 0); // original
}

void CoordinateConverter::carthToPolar(QVector4D p, double &theta, double &phi)
{
    phi = qAtan(p.y() / p.x()); // original
    theta =  qAcos(p.z()); // original

    theta = p.z() == 0 ? 0 : qAtan(qSqrt(p.x() * p.x() + p.y() * p.y()) / p.z());

    if(p.x() == 0)
    {

        if(p.y() >= 0)
        {
            phi = 0.5 * M_PI;
        }
        else
        {

            phi = -0.5 * M_PI;
        }
    }
    else
    {
        phi = qAtan(p.y()/p.x());
    }

}


void CoordinateConverter::carthToImageSpace(QVector4D c, double &outY, double &outX)
{
    carthToPolar(c, outY, outX);


    outX = outX / M_PI * float(width/2);
    outY = outY / 2. / M_PI * float(width);


    if(outX < 0)
    {
         outX += width/2;
    }

    if(outY < 0)
    {
         outY += width / 2;
    }

    if(c.y() < 0)
    {
         outX += width / 2;
    }
}

void CoordinateConverter::rotate(double ix, double iy, double &outX, double &outY)
{
    bool showDebug = false;
    bool isLowerY = iy < width;
    if(showDebug) qDebug() << "Handle " << ix << iy;
    ix = ix / width * (2. * M_PI);
    iy = iy / (width / 2.) * M_PI;


    if(showDebug) qDebug() << " theta, phi " << ix << iy;
    QVector4D c = polarToCarth(iy, ix); // polarToCarth(ix, iy);
    if(showDebug) qDebug() << " as carth " << c;
    c = m * c;

    carthToImageSpace(c, outY, outX);

    /*
    outY = outY / M_PI * float(width/2);
    outX = outX / 2. / M_PI * float(width);


    if(outY < 0)
    {
         outY += width/2;
    }

    if(outX < 0)
    {
         outX += width / 2;
    }


    if(c.y() < 0)
    {
         outX += width / 2;
    }
    */



    if(showDebug) qDebug() << " rotated" << c << " image space " << outX << outY;
}

