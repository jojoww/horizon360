#ifndef COORDINATECONVERTER_H
#define COORDINATECONVERTER_H

#include <QMatrix4x4>
#include <QVector3D>
#include <QVector4D>

class CoordinateConverter
{
private:
    double width;
    double dist;
    QMatrix4x4 m;
public:
    void carthToPolar(QVector4D p, double &theta, double &phi);
    QVector4D polarToCarth(double theta, double phi);
    CoordinateConverter();
    void setWidth(int width);
    QMatrix4x4 rotationMatrix(QVector3D axis, double angle);
    void setAngle(double angleX, double angleY, double angleZ);
    void rotate(double ix, double iy, double &outX, double &outY);
    void carthToImageSpace(QVector4D c, double &outX, double &outY);
};

#endif // COORDINATECONVERTER_H
