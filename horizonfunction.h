#ifndef HORIZONFUNCTION_H
#define HORIZONFUNCTION_H
#include <QImage>

class HorizonFunction
{
public:
    HorizonFunction();

    double getShift() const;
    void setShift(double shift);

    double getAmplitude() const;
    void setAmplitude(double amplitude);
    
    QImage getImage(int width);
    double getDerivative(int x, int width);
    double getY(int x, int width);
private:
    double m_shift;
    double m_amplitude;
    double translateX(int x, int width);
};

#endif // HORIZONFUNCTION_H
