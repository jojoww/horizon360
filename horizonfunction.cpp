#include "horizonfunction.h"
#include <QDebug>

#include <QtMath>

HorizonFunction::HorizonFunction()
{
    m_shift = 0;
    m_amplitude = 0;
}

double HorizonFunction::getShift() const
{
    return m_shift;
}

void HorizonFunction::setShift(double shift)
{
    qDebug() << "Horizon - updated shift: " << shift;
    m_shift = shift;
}

double HorizonFunction::getAmplitude() const
{
    return m_amplitude;
}

void HorizonFunction::setAmplitude(double amplitude)
{
    qDebug() << "Horizon - updated amplitude: " << amplitude;
    m_amplitude = amplitude;
}

double HorizonFunction::translateX(int x, int width)
{
    return double(x) / double(width) * M_PI * 2. + m_shift / 360. * M_PI * 2.;
}

double HorizonFunction::getY(int x, int width)
{
    return qSin(translateX(x, width)) * m_amplitude/180.*double(width/4) + double(width/4);
}


double HorizonFunction::getDerivative(int x, int width)
{
    return qCos(translateX(x, width))  * m_amplitude/180.;
}


QImage HorizonFunction::getImage(int width)
{
    QImage image = QImage(width, width/2, QImage::Format_ARGB32);
    for(int x = 0; x < width; x++)
    {
        int y = getY(x, width);
        image.setPixel(x, y, QColor(255, 0, 0).rgba());
        //qDebug() << "Horizon - create image " << x << y;
    }

    return image;
}
